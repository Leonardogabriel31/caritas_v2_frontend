import axios from "axios";
import { Button, Label, Modal, TextInput } from "flowbite-react";
import React, { Fragment, useState } from "react";
import { toast } from "react-toastify";


export default function CreateState({chargerData}) {
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    name: "",
  });

  const handleChange = (e) => {
    setForm({name: e.target.value});
  };

  const modalShow = () => {
    setShow(true);
  };

  const modalHide = () => {
    setShow(false);
  };

  const createDataState = async (e) => {
    // console.log(form);

    await axios.post('states/', form).then((res) => {
      toast.success('Estado Creado', {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        });
      chargerData()
      modalHide();
    })
  };


  return (
    <Fragment>
      <Button onClick={modalShow} color={"success"}>Crear Estado</Button>
      <Modal show={show} onClose={modalHide}>
        <Modal.Header>Crear Estado</Modal.Header>
        <Modal.Body>
          <form className="flex flex-col gap-4">
            <div>
              <div className="mb-2 block">
                <Label htmlFor="name_state" value="Nombre del Estado" />
              </div>
              <TextInput
                id="name_state"
                type="text"
                placeholder="Introduzca el nombre del Estado"
                required={true}
                onChange={handleChange}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" onClick={(e) => createDataState(e)}>
            Crear
          </Button>
          
          <Button color="gray" onClick={modalHide}>
            Cancelar
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
