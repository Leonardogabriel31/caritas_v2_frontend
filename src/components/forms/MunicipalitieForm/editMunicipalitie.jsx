import axios from "axios";
import { Button, Label, Modal, TextInput } from "flowbite-react";
import React, { Fragment, useState } from "react";
import { AiFillEdit } from "react-icons/ai";

export default function EditMunicipalitie({chargerData, formState}) {
  // export default function EditState() {
  const [show, setShow] = useState(false);
  const [form, setForm] = useState(formState);

  const handleChange = (e) => {
    setForm({id: form.id, name: e.target.value});
  };

  const modalHide = () => {
    setShow(false);
  };


  const modalShow = () => {
    setShow(true);
  }

  const editDataMunicipalitie = async(data) => {
    console.log(form);

    await axios.put(`municipalities/${form.id}`,form,).then((res) => {
      console.log(res);
    });
    chargerData();
    modalHide();
  };
  
  return (
    <Fragment>
        <Button onClick={() => modalShow()}> 
            <AiFillEdit />
        </Button>
        <Modal show={show} onClose={modalHide}>
            <Modal.Header>Editar Municipio</Modal.Header>
        <Modal.Body>
            <form className="flex flex-col gap-4">
                <div>
                <div className="mb-2 block">
                    <Label htmlFor="name_municipalitie" value="Nombre del Municipio" />
                </div>
                <TextInput
                    id="name_municipalitie"
                    type="text"
                    placeholder="Introduzca el nombre del Municipio"
                    value={form.name}
                    required={true}
                    onChange={handleChange}
                />
                </div>
            </form>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" onClick={(data) => editDataMunicipalitie(data)}>
            Editar
          </Button>
          <Button color="gray" onClick={modalHide}>
            Cancelar
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
