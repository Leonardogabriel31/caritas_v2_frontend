import axios from "axios";
import { Button, Label, Modal, TextInput } from "flowbite-react";
import React, { Fragment, useState } from "react";


export default function CreateMunicipalitie({chargerDataMuni}) {
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    name: "",
  });

  const handleChange = (e) => {
    setForm({name: e.target.value});
  };

  const modalShow = () => {
    setShow(true);
  };

  const modalHide = () => {
    setShow(false);
  };

  const createDataMunicipalitie = async (e) => {
    // console.log(form);

    await axios.post('municipalities/', form)
    chargerDataMuni()
    modalHide();
  };


  return (
    <Fragment>
      <Button onClick={modalShow} color={"success"}>Crear Municipio</Button>
      <Modal show={show} onClose={modalHide}>
        <Modal.Header>Crear Municipio</Modal.Header>
        <Modal.Body>
          <form className="flex flex-col gap-4">
            <div>
              <div className="mb-2 block">
                <Label htmlFor="name_municipalitie" value="Nombre del Municipio" />
              </div>
              <TextInput
                id="name_municipalitie"
                type="text"
                placeholder="Introduzca el nombre del Municipio"
                required={true}
                onChange={handleChange}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" onClick={(e) => createDataMunicipalitie(e)}>
            Crear
          </Button>
          
          <Button color="gray" onClick={modalHide}>
            Cancelar
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
