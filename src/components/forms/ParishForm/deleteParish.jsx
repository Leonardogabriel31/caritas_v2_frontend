import axios from "axios";
import { Button, Modal } from "flowbite-react";
import { Fragment, useState } from "react";
import { AiFillDelete, AiOutlineExclamationCircle } from "react-icons/ai";

export default function DeleteParish({ chargerData, formState }) {
  const [show, setShow] = useState(false);
  const [form] = useState(formState);

  const modalShow = () => {
    setShow(true);
  };

  const modalHide = () => {
    setShow(false);
  };

  const deleteDataParish = async (data) => {
    await axios.delete(`parishes/${form.id}`).then((res) => {
        // console.log(res.data.msg);
    }).catch((e) => {
        console.log(e.response.data.msg);
    });
    chargerData();
    modalHide();
  };

  return (
    <Fragment>
      <Button color={"failure"} onClick={() => modalShow()}>
        <AiFillDelete />
      </Button>
      <Modal size="md" popup={true} show={show} onClose={modalHide}>
        <Modal.Header />
        <Modal.Body>
          <div className="text-center">
            <AiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
            <h3 className="mb-5 text-lg font-normal">
              Esta seguro que desea eliminar la Parroquia{" "}
              <span className="font-medium">{`${form.name}`}</span> ?
            </h3>
            <div className="flex justify-center gap-4">
              <Button
                color={"failure"}
                type="submit"
                onClick={(data) => deleteDataParish(data)}
              >
                Eliminar
              </Button>
              <Button color="gray" onClick={modalHide}>
                Cancelar
              </Button>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </Fragment>
  );
}
