import axios from "axios";
import { Button, Label, Modal, TextInput } from "flowbite-react";
import React, { Fragment, useState } from "react";


export default function CreateParish({chargerData}) {
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    name: "",
  });

  const handleChange = (e) => {
    setForm({name: e.target.value});
  };

  const modalShow = () => {
    setShow(true);
  };

  const modalHide = () => {
    setShow(false);
  };

  const createDataParish = async (e) => {
    // console.log(form);

    await axios.post('parishes/', form)
    chargerData()
    modalHide();
  };


  return (
    <Fragment>
      <Button onClick={modalShow} color={"success"}>Crear Parroquia</Button>
      <Modal show={show} onClose={modalHide}>
        <Modal.Header>Crear Parroquia</Modal.Header>
        <Modal.Body>
          <form className="flex flex-col gap-4">
            <div>
              <div className="mb-2 block">
                <Label htmlFor="name_parish" value="Nombre de la Parroquia" />
              </div>
              <TextInput
                id="name_parish"
                type="text"
                placeholder="Introduzca el nombre de la Parroquia"
                required={true}
                onChange={handleChange}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit" onClick={(e) => createDataParish(e)}>
            Crear
          </Button>
          
          <Button color="gray" onClick={modalHide}>
            Cancelar
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
