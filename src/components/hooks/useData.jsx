import { useState } from "react";

export default function useData() {
  const getData = () => {
    const dataString = localStorage.getItem("token");
    const userData = JSON.parse(dataString);
    return userData;
  };

  const [data, setData] = useState(getData());

  const saveData = (userData) => {
    localStorage.setItem("token", JSON.stringify(userData.token));
    localStorage.setItem("refresh", JSON.stringify(userData.refresh));
    localStorage.setItem("user", JSON.stringify(userData.user));
    setData(userData.token);
  };

  return {
    setData: saveData,
    data,
  };
}
