import { Avatar, Dropdown, Navbar, Sidebar } from "flowbite-react";
import React from "react";
import { AiFillHome, AiOutlineHome } from "react-icons/ai";
import { BiChurch } from "react-icons/bi";
import { BsBagPlus } from "react-icons/bs";
import { FiMapPin } from "react-icons/fi";
import { MdChildCare, MdFamilyRestroom } from "react-icons/md";
import { Link, Outlet } from "react-router-dom";
import logo from '../../static/img/LogoCaritas.png';
import icon from '../../static/img/Favicon.png';

export default function NavbarIndex() {
  return (
    <>
      <div className="border flex-initial">
        <Navbar fluid={true} rounded={true}>
          <Navbar.Brand>
            <Link to={"/"}>
              <img
                src={logo}
                className="mr-3 h-14 pr-12"
                alt="Logo Caritas"
              />
            </Link>
              <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white pl-16">
                Caritas
              </span>
          </Navbar.Brand>
          <div className="flex md:order-2">
            <Dropdown
              arrowIcon={false}
              inline={true}
              label={
                <Avatar
                  alt="User settings"
                  img={icon}
                  rounded={true}
                />
              }
            >
              <Dropdown.Header>
                <span className="block text-sm">Bonnie Green</span>
                <span className="block truncate text-sm font-medium">
                  name@flowbite.com
                </span>
              </Dropdown.Header>
              <Dropdown.Item>Dashboard</Dropdown.Item>
              <Dropdown.Item>Settings</Dropdown.Item>
              <Dropdown.Item>Earnings</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item>Sign out</Dropdown.Item>
            </Dropdown>
            <Navbar.Toggle />
          </div>
        </Navbar>
      </div>
      <div className="flex">
        <div className="flex-initial bg-white h-screen border">
          <Sidebar>
            <Sidebar.Items>
              <Sidebar.ItemGroup>
                <Link to="/">
                  <Sidebar.Item icon={AiFillHome}>Dashboard</Sidebar.Item>
                </Link>

                <Sidebar.Collapse icon={FiMapPin} label="Ubicacion">
                  <Link to="/state">
                    <Sidebar.Item>Estados</Sidebar.Item>
                  </Link>

                  <Link to="/municipalitie">
                    <Sidebar.Item>Municipios</Sidebar.Item>
                  </Link>

                  <Link to="/parish">
                    <Sidebar.Item>Parroquias</Sidebar.Item>
                  </Link>

                  <Link to="community">
                    <Sidebar.Item>Comunidades</Sidebar.Item>
                  </Link>

                  <Link to="sector">
                    <Sidebar.Item>Sectores</Sidebar.Item>
                  </Link>
                </Sidebar.Collapse>

                <Sidebar.Collapse icon={AiOutlineHome} label="Viviendas">
                  <Link to="/child1">
                    <Sidebar.Item>Viviendas</Sidebar.Item>
                  </Link>
                </Sidebar.Collapse>

                <Sidebar.Collapse
                  icon={MdFamilyRestroom}
                  label="Representantes"
                >
                  <Link to="/parents1">
                    <Sidebar.Item>Representantes1</Sidebar.Item>
                  </Link>
                </Sidebar.Collapse>

                <Sidebar.Collapse icon={MdChildCare} label="Niños">
                  <Link to="/child1">
                    <Sidebar.Item>Child1</Sidebar.Item>
                  </Link>
                </Sidebar.Collapse>

                <Sidebar.Collapse icon={BsBagPlus} label="Insumos">
                  <Link to="/insume1">
                    <Sidebar.Item>Insumos1</Sidebar.Item>
                  </Link>
                </Sidebar.Collapse>

                <Sidebar.Collapse icon={BiChurch} label="Iglesias">
                  <Link to="/church1">
                    <Sidebar.Item>Iglesia1</Sidebar.Item>
                  </Link>
                </Sidebar.Collapse>
              </Sidebar.ItemGroup>
            </Sidebar.Items>
          </Sidebar>
        </div>
        <div className="flex-1">
          <Outlet />
        </div>
      </div>
    </>
  );
}
