import { Avatar, Button, TextInput } from "flowbite-react";
import icon from "../static/img/Favicon.png";
import React, { Fragment, useState } from "react";
import { PropTypes } from "prop-types";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";

export default function Login({ setData }) {
  const [form, setForm] = useState({ username: "", password: "" });

  const handleSubmit = async (e) => {
    e.preventDefault()
    console.log(form);

    await axios
      .post("login", form)
      .then((res) => {
        setData(res.data);
      })
      .catch((e) => {
        toast.error(e.response.data.msg, {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          });
      });
  };

  return (
    <Fragment>
      <div className="flex justify-center pt-60">
        <form
          className="border-stone-200 rounded-lg border-4 p-8 lg:w-1/3 relative shadow-2xl"
          onSubmit={handleSubmit}
        >
          <div className="flex justify-center ">
            <div className="absolute -top-12">
              <Avatar
                alt="User settings"
                img={icon}
                size="lg"
                bordered={true}
                rounded={true}
              />
            </div>
          </div>
          <div className="py-2">
            <TextInput
              type="text"
              placeholder="Usuario"
              required={true}
              onChange={(e) =>
                setForm({ username: e.target.value, password: form.password })
              }
            />
          </div>
          <div className="py-2">
            <TextInput
              type="password"
              required={true}
              placeholder="Contraseña"
              onChange={(e) =>
                setForm({ username: form.username, password: e.target.value })
              }
            />
          </div>
          <div className="flex justify-center pt-4">
            <Button type="submit">Iniciar Sesión</Button>
          </div>
        </form>
      </div>
      <ToastContainer />
    </Fragment>
  );
}

Login.propTypes = {
  setData: PropTypes.func.isRequired,
};
