import React, { useEffect, useState } from 'react'
import { Select} from 'flowbite-react'
import axios from 'axios';

export const SelectMunicipalitie = ({selectMunicipalitie}) => {
  const [selectsMunicipalitie, setSelectsMunicipalitie] = useState([]);



  useEffect(() => {
    chargerMunicipalitie(selectMunicipalitie)
  }, [selectMunicipalitie]);




  const chargerMunicipalitie = async () => {
    axios.get("municipalities/").then((respuesta) => {
      // console.log(respuesta);
      setSelectsMunicipalitie(respuesta.data)
    })
    .catch((error) => {
      console.log(error);

    })
  };


  return (
    <div>
      <Select id='selectMunicipalitie' name='selectMunicipalitie'>
        <option key='0' value='0'>
          Seleccione
        </option>
        {selectsMunicipalitie.map((selectOption) => (
          <option key={selectOption.id} value={selectOption.id}>
            {selectOption.name}
          </option>
        ))}
      </Select>
    </div> 
  )
}