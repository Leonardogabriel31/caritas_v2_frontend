import React, { useEffect, useState } from 'react'
import { Select} from 'flowbite-react'
import axios from 'axios';
import { SelectState } from './selectState';

export const Selects = ({onChange}) => {
  const [selectsState, setSelectsState] = useState([]);

  useEffect(() => {
    chargerState();
  }, []);

  const chargerState = async () => {
    axios.get("states/").then((respuesta) => {
      // console.log(respuesta);
      setSelectsState(respuesta.data)
    })
    .catch((error) => {
      console.log(error);
    })
  }

  // const chargerState = async () => {
  //   axios.get("states/").then((respuesta) => {
  //     // console.log(respuesta);
  //     setSelectsState(respuesta.data)
  //   })
  //   .catch((error) => {
  //     console.log(error);

  //   })
  // };


  return (
    <div>
      <SelectState />
      {/* <form name='formularioSelect'>
        <Select id='selectState' name='selectState' onChange={onChange}>
          <option key='0' value='0'>
            Seleccione
          </option>
          {selectsState.map((selectOption) => (
            <option key={selectOption.id} value={selectOption.id}>
              {selectOption.name}
            </option>
          ))}
        </Select>
      </form> */}
</div> 
)
}