import React, { useEffect, useState } from 'react'
import { Select} from 'flowbite-react'
import axios from 'axios';

export const SelectState = ({onChange = () => {}}) => {
  const [selectsState, setSelectsState] = useState([]);



  useEffect(() => {
    chargerState();
  }, []);




  const chargerState = async () => {
    axios.get("states/").then((respuesta) => {
      // console.log(respuesta);
      setSelectsState(respuesta.data)
    })
    .catch((error) => {
      console.log(error);

    })
  };


  return (
    <div>

        <Select id='selectState' name='selectState' onChange={onChange}>
          <option key='0' value='0'>
            Seleccione
          </option>
          {selectsState.map((selectOption) => (
            <option key={selectOption.id} value={selectOption.id}>
              {selectOption.name}
            </option>
          ))}
        </Select>

</div> 
)
}
