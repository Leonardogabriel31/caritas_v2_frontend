import axios from "axios";
const dataString = localStorage.getItem("token");
const token = JSON.parse(dataString);

axios.defaults.baseURL = 'http://localhost:8000/api/';
axios.defaults.headers['Content-Type'] = 'application/json';
axios.defaults.headers.common = {'Authorization': `Bearer ${token}`}
