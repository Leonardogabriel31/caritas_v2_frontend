import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import { Table } from "flowbite-react";
import CreateState from "../../../components/forms/StateForm/createState";
import EditState from "../../../components/forms/StateForm/editState";
import DeleteState from "../../../components/forms/StateForm/deleteState";
import { ToastContainer } from "react-toastify";

const State = () => {
  const [states, setStates] = useState([]);

  useEffect(() => {
    chargerData();
  }, []);

  const chargerData = async () => {
    const respuesta = await axios.get("states/");
    // console.log(respuesta.data);
    setStates(respuesta.data);
  };

  return (
    <Fragment>
      <div className="flex justify-around py-4">
        <span className="self-center whitespace-nowrap text-4xl font-semibold dark:text-white pl-16">
          Estados
        </span>
        <div className="col-end-2 col-span-1 pt-5">
          <CreateState chargerData={chargerData} />
        </div>
      </div>
      <div className="flex justify-center">
        <div className="lg:w-2/3 shadow-2xl">
          <Table striped={true}>
            <Table.Head>
              <Table.HeadCell className="flex justify-center">
                Nombre del Estado
              </Table.HeadCell>
              <Table.HeadCell>
                <span className="flex justify-center">Opciones</span>
              </Table.HeadCell>
            </Table.Head>
            <Table.Body className="divide-y">
              {states.map((state) => (
                <Table.Row
                  className="bg-white dark:border-gray-700 dark:bg-gray-800"
                  key={state.id}
                >
                  <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white flex justify-center">
                    {state.name}
                  </Table.Cell>
                  <Table.Cell>
                    <div className="flex justify-center">
                      <EditState chargerData={chargerData} formState={state} />
                      <DeleteState
                        chargerData={chargerData}
                        formState={state}
                      />
                    </div>
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </div>
        <ToastContainer />
      </div>
    </Fragment>
  );
};

export default State;
