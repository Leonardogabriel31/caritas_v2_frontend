import React, { useEffect, useState } from "react";
import axios from "axios";
import { Label, Table } from "flowbite-react";
import CreateMunicipalitie from "../../../components/forms/MunicipalitieForm/createMunicipalitie";
import EditMunicipalitie from "../../../components/forms/MunicipalitieForm/editMunicipalitie";
import DeleteMunicipalitie from "../../../components/forms/MunicipalitieForm/deleteMunicipalitie";
import { Selects } from "../../../components/Select/Select";



const Municipalitie = () => {

  const [municipalities, setMunicipalities] = useState([]);
  const [listMunicipalities, setListMunicipalities] = useState([]);

  useEffect(() => {
    chargerDataMuni();
  }, []);


  const handleChange = (e) => {
    e.preventDefault();
    const {value} = e.target;
    const municipality = municipalities.filter((muni) =>(
      muni.state_id === parseInt(value)
    ))
    setListMunicipalities(municipality)
  }
  

  const chargerDataMuni = async () => {
    const respuesta = await axios.get("municipalities/");
    // console.log(respuesta.data);
    setMunicipalities(respuesta.data);
  };

  return (
    <div className="grid grid-cols-6 gap-4">
      <div className="col-end-2 col-span-1 pt-5">
        <CreateMunicipalitie chargerDataMuni={chargerDataMuni}/>
      </div>
      <div className="col-start-3 col-span-2">
        <Label
          htmlFor="selectState"
          value="Seleccione un Estado"
        />
        <Selects onChange={handleChange}/>
      </div>
      
      <div className="col-start-2 col-span-4">
        <Table striped={true} id='municipalitieTable'>
          <Table.Head>
            <Table.HeadCell className="flex justify-center">
              Nombre del Municipio
            </Table.HeadCell>
            <Table.HeadCell>
              <span>Opciones</span>
            </Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {listMunicipalities.map((municipalitie) => (
              <Table.Row
                className="bg-white dark:border-gray-700 dark:bg-gray-800"
                key={municipalitie.id}>
                <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white flex justify-center">
                  {municipalitie.name}
                </Table.Cell>
                <Table.Cell>
                  <div className="flex">
                    <EditMunicipalitie chargerDataMuni={chargerDataMuni} formState={municipalitie} />
                    <DeleteMunicipalitie chargerDataMuni={chargerDataMuni} formState={municipalitie} />
                  </div>
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </div>
    </div>
  );
};

export default Municipalitie;
