import React, { useEffect, useState } from "react";
import axios from "axios";
import { Label, Table } from "flowbite-react";

import EditState from "../../../components/forms/StateForm/editState";
import DeleteState from "../../../components/forms/StateForm/deleteState";
import CreateParish from "../../../components/forms/ParishForm/createParish";
// import { Selects } from "../../../components/Select/Select";
import { SelectState } from "../../../components/Select/selectState";
import { SelectMunicipalitie } from "../../../components/Select/selectMunicipalitie";

const Parish = () => {
  const [parishes, setParishes] = useState([]);
  const [formValues, setFormValues] = useState({});

  useEffect(() => {
    chargerData();
  }, []);


  const handleChange = (e) => {
    e.preventDefault();
    const {value, name} = e.target;
    setFormValues({...formValues, [name]:value });
  }

  // console.log('formValues', formValues)

  const chargerData = async () => {
    const respuesta = await axios.get("parishes/");
    // console.log(respuesta.data);
    setParishes(respuesta.data);
  };

  return (
    <div className="grid grid-cols-6 gap-4">
      <div className="col-end-2 col-span-1 pt-5">
        <CreateParish chargerData={chargerData} />
      </div>
      <div className="col-start-3 col-span-2">
        <Label
          htmlFor="selectState"
          value="Seleccione un Estado"
        />
        <SelectState onChange={handleChange}/>
        <br />
        <Label
          htmlFor="selectMunicipalitie"
          value="Seleccione un Municipio"
        />
        <SelectMunicipalitie selectMunicipalitie={formValues.selectMunicipalitie}/> 
      </div>
      <div className="col-start-2 col-span-4">
        <Table striped={true}>
          <Table.Head>
            <Table.HeadCell className="flex justify-center">
              Nombre de la Parroquia
            </Table.HeadCell>
            <Table.HeadCell>
              <span>Opciones</span>
            </Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {parishes.map((parish) => (
              <Table.Row
                className="bg-white dark:border-gray-700 dark:bg-gray-800"
                key={parish.id}
              >
                <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white flex justify-center">
                  {parish.name}
                </Table.Cell>
                <Table.Cell>
                  <div className="flex">
                    <EditState chargerData={chargerData} formState={parish} />
                    <DeleteState chargerData={chargerData} formState={parish} />
                  </div>
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </div>
    </div>
  );
};

export default Parish;
