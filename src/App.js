import Dashboard from "./Pages/Dashboard";
import NavbarIndex from "./components/Navbar/NavbarIndex";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import State from "./Pages/Ubicate/states/State";

import Municipalitie from "./Pages/Ubicate/municipalitie/Municipalitie";

import Parish from "./Pages/Ubicate/parish/Parish";
import Community from "./Pages/Ubicate/community/Community";
import Sector from "./Pages/Ubicate/sector/Sector";
import ParentsDate from "./Pages/ParentsDate/ParentsDate";
import ChildDate from "./Pages/Childs/ChildDate";
import Insumes from "./Pages/Insumes/Insumes";
import ChurchDate from "./Pages/Church/ChurchDate";
import Login from "./components/login";
import useData from "./components/hooks/useData";
import axios from "axios";

function App() {
  const { data, setData } = useData();
  if (!data) {
    return <Login setData={setData} />;
  }else {
    axios.defaults.headers.common = {'Authorization': `Bearer ${data}`}
  }
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<NavbarIndex />}>
          <Route path="/" element={<Dashboard />} />

          {/* UBICACION */}
          <Route path="/state" element={<State />} />

          <Route path="/municipalitie" element={<Municipalitie />} />

          <Route path="/parish" element={<Parish />} />

          <Route path="/community" element={<Community />} />

          <Route path="/sector" element={<Sector />} />

          {/* REPRESENTANTES */}

          <Route path="/parents1" element={<ParentsDate />} />

          {/* NIÑOS */}

          <Route path="/child1" element={<ChildDate />} />

          {/* INSUMOS */}

          <Route path="/insume1" element={<Insumes />} />

          {/* IGLESIAS */}

          <Route path="/church1" element={<ChurchDate />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
